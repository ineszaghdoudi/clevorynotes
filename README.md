# CleveryNotes



## Architercute
<img src="/images/Architecture.gif" alt="architecture"/>


The CleveryNotes project is developed using a microservices architecture to ensure scalability, modularity, and maintainability. The architecture consists of the following microservices:

### 1. Frontend Microservice <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/512px-Angular_full_color_logo.svg.png?20160527092314" alt="angular" width="50" height="50" />
Provides a user-friendly todo list interface to manage tasks. It renders the user interface, handles user interactions, and communicates with backend services.

### 2. Authentication Microservice <img src="https://spring.io/img/logos/spring-initializr.svg" alt="spring boot" width="50" height="50" />
Handles user authentication within the todo application. It manages user registration, login functionality, and token-based authentication. It communicates with the MongoDB database.

### 3. Todo management Microservice <img src="https://spring.io/img/logos/spring-initializr.svg" alt="spring boot" width="50" height="50" />
Manages user todos and handles CRUD operations for todos. It communicates with the MongoDB database for data storage and retrieval.

## Getting started

To run the CleveryNotes project locally, follow these steps:

1.Clone the repository:
```
git clone https://gitlab.com/ineszaghdoudi/clevorynotes.git
```
2.Install the required dependencies for each microservice:
```
cd Frontend
npm install

cd MicroserviceAuthentication
./mvnw install

cd MicroserviceTodoManagement
./mvnw install

```
3.Start the microservices
```
cd Frontend
npm start

cd MicroserviceAuthentication
./mvnw spring-boot:run

cd MicroserviceTodoManagement
./mvnw spring-boot:run

```
4.Access the application at http://localhost:4200 in your web browser.

## Screenshots

<img src="/images/login.png" alt="architecture"/>
<img src="/images/register.png" alt="architecture"/>


