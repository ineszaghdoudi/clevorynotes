package com.notes.cleverynotes.controller;

import com.notes.cleverynotes.consumer.RabbitMQConsumer;
import com.notes.cleverynotes.interfaces.ITodoService;
import com.notes.cleverynotes.model.Todo;
import com.notes.cleverynotes.repository.TodoRepository;
import com.notes.cleverynotes.service.DbSequenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/clevery/todo")
@CrossOrigin(origins = "*")
@RestController
public class TodoController {

    @Autowired
    private ITodoService iTodoService;

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    DbSequenceService dbSequenceService;


    @GetMapping("/retrieve-all-todos/{token}")
    public List<Todo> getTodos(@PathVariable("token") String token) {
        List<Todo> listTodos = iTodoService.retrieveAllTodos(token);

        return listTodos;
    }

    @GetMapping("/retrieve-todo/{id-todo}")
    public Optional<Todo> getTodoById(@PathVariable("id-todo") long id) {
        return iTodoService.retrieveTodo(id);
    }

    @PostMapping("/add-todo/{token}")
    public Todo addTodo(@RequestBody Todo todo, @PathVariable("token") String token) {

        todo.setId(dbSequenceService.generateSequence(Todo.SEQUENCE_NAME));
        todo.setToken(token);
        Todo t= iTodoService.addTodo(todo, token);
        return todo;
    }

    @PutMapping("/update-todo/{token}")
    public Todo updateTodo(@RequestBody Todo todo, @PathVariable("token") String token) {
        Todo t= iTodoService.updateTodo(todo, token);
        return t;
    }

    @DeleteMapping("/remove-todo/{id-todo}")
    public void removeTodo(@PathVariable("id-todo") long id) {
        iTodoService.removeTodo(id);
    }








}