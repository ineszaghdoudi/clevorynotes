package com.notes.cleverynotes.repository;

import com.notes.cleverynotes.dto.LoginDTO;
import com.notes.cleverynotes.model.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoginDTORepository extends MongoRepository<LoginDTO,String> {
        LoginDTO findByToken(String token);
        void deleteByUsername(String s);
}