package com.notes.cleverynotes.repository;

import com.notes.cleverynotes.model.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends MongoRepository<Todo,Long> {
    List<Todo> findByUsername(String username);
}