package com.notes.cleverynotes.interfaces;

import com.notes.cleverynotes.model.Todo;

import java.util.List;
import java.util.Optional;

public interface ITodoService {
    List<Todo> retrieveAllTodos(String token);
    Optional<Todo> retrieveTodo(long id);
    Todo addTodo(Todo todo, String token);
    Todo updateTodo(Todo todo, String token);
    void removeTodo(long id);


}
