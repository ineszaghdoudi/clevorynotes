package com.notes.cleverynotes.consumer;

import com.notes.cleverynotes.dto.LoginDTO;
import com.notes.cleverynotes.model.Todo;
import com.notes.cleverynotes.repository.LoginDTORepository;
import com.notes.cleverynotes.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQConsumer {

    @Autowired
    LoginDTORepository loginDTORepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConsumer.class);


    @RabbitListener(queues = "${rabbitmq.queue.name}")
    public void receiveMessage(LoginDTO loginDTO) {
        LOGGER.info("Received message from RabbitMQ: {}", loginDTO);
        loginDTORepository.save(loginDTO);
    }

    @RabbitListener(queues = "${rabbitmq.queue.name.logout}")
    public void receiveLogoutMessage(LoginDTO loginDTO) {
        LOGGER.info("Received Logout message from RabbitMQ: {}", loginDTO);
        String username=loginDTO.getUsername();
        loginDTORepository.deleteByUsername(username);
    }


}