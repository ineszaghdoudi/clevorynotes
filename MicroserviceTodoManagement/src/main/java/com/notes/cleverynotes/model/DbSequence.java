package com.notes.cleverynotes.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.persistence.Id;

@Document("db_sequence")
@Component
@Getter
@NoArgsConstructor
@Setter
public class DbSequence {

    @Id
    private String id;

    private int seq;


}
