package com.notes.cleverynotes.model;


import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;

import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Transient;
import java.io.Serializable;
import java.security.PrivateKey;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Document("todos")
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Todo implements Serializable {

    @Transient
    public static final String SEQUENCE_NAME = "todo_sequence";

    @Id
    private long id;

    private String title;

    private String description;

    private LocalDate creationDate;

    private LocalDate dueDate;

    private boolean completed;

    private String category;

    private int priority;

    private LocalDate reminderDateTime;

    private String username;

    private String token;



}