package com.notes.cleverynotes.service;

import com.notes.cleverynotes.consumer.RabbitMQConsumer;
import com.notes.cleverynotes.dto.LoginDTO;
import com.notes.cleverynotes.interfaces.ITodoService;
import com.notes.cleverynotes.model.Todo;

import com.notes.cleverynotes.repository.LoginDTORepository;
import com.notes.cleverynotes.repository.TodoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service
public class TodoService implements ITodoService {

    @Autowired
    TodoRepository todoRepository;

    @Autowired
    RabbitMQConsumer rabbitMQConsumer;

    @Autowired
    LoginDTORepository loginDTORepository;


    @Override
    public List<Todo> retrieveAllTodos(String token) {
        String username = null;

        List<LoginDTO> loginDTOs = loginDTORepository.findAll();

        LoginDTO loginDTO = loginDTORepository.findByToken(token);
        if (loginDTO != null) {

            username = loginDTO.getUsername();
        }

        return (List<Todo>) todoRepository.findByUsername(username);
    }




    @Override
    public Optional<Todo> retrieveTodo(long id) {
        return todoRepository.findById(id);
    }

    @Override
    public Todo addTodo(Todo todo, String token) {
        String username = null;

        List<LoginDTO> loginDTOs = loginDTORepository.findAll();
        LoginDTO loginDTO = loginDTORepository.findByToken(token);

        if (loginDTO != null) {

            username = loginDTO.getUsername();

            todo.setUsername(username);
            todo.setCreationDate(LocalDate.now());

            todoRepository.save(todo);

        }

        return todo;
    }




    @Override
    public Todo updateTodo(Todo todo, String token) {
        String username = null;

        List<LoginDTO> loginDTOs = loginDTORepository.findAll();
        LoginDTO loginDTO = loginDTORepository.findByToken(token);

        if (loginDTO != null) {

            username = loginDTO.getUsername();

            todo.setUsername(username);
            todo.setToken(token);
        }
            return todoRepository.save(todo);

    }

    public void removeTodo(long id) {
        todoRepository.deleteById(id);
    }




}