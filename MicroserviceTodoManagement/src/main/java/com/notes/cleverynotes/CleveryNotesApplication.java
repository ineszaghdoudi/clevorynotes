package com.notes.cleverynotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleveryNotesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CleveryNotesApplication.class, args);
    }

}
