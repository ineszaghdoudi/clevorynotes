package com.notes.cleverynotes.test.service;

import com.notes.cleverynotes.dto.LoginDTO;
import com.notes.cleverynotes.model.Todo;
import com.notes.cleverynotes.repository.LoginDTORepository;
import com.notes.cleverynotes.repository.TodoRepository;
import com.notes.cleverynotes.service.TodoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class TodoServiceTests {

    @Mock
    private TodoRepository todoRepository;

    @Mock
    private LoginDTORepository loginDTORepository;

    @InjectMocks
    private TodoService todoService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRetrieveAllTodos() {
        // Prepare test data
        String token = "sample_token";
        String username = "testUser";
        List<Todo> expectedTodos = Collections.singletonList(new Todo());

        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username);
        when(loginDTORepository.findByToken(token)).thenReturn(loginDTO);

        when(todoRepository.findByUsername(username)).thenReturn(expectedTodos);

        List<Todo> actualTodos = todoService.retrieveAllTodos(token);

        assertEquals(expectedTodos, actualTodos);
        verify(loginDTORepository, times(1)).findByToken(token);
        verify(todoRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testRetrieveTodo() {

        long todoId = 1;
        Todo expectedTodo = new Todo();

        when(todoRepository.findById(todoId)).thenReturn(Optional.of(expectedTodo));

        Optional<Todo> actualTodo = todoService.retrieveTodo(todoId);

        assertEquals(expectedTodo, actualTodo.orElse(null));
        verify(todoRepository, times(1)).findById(todoId);
    }

    @Test
    public void testAddTodo() {
        String token = "sample_token";
        String username = "testUser";
        Todo todo = new Todo();


        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username);
        when(loginDTORepository.findByToken(token)).thenReturn(loginDTO);


        when(todoRepository.save(any(Todo.class))).thenReturn(todo);


        Todo addedTodo = todoService.addTodo(todo, token);


        assertEquals(todo, addedTodo);
        assertEquals(username, todo.getUsername());
        verify(loginDTORepository, times(1)).findByToken(token);
        verify(todoRepository, times(1)).save(todo);
    }

    @Test
    public void testUpdateTodo() {

        String token = "sample_token";
        String username = "testUser";
        Todo todo = new Todo();


        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username);
        when(loginDTORepository.findByToken(token)).thenReturn(loginDTO);

        when(todoRepository.save(any(Todo.class))).thenReturn(todo);

        Todo updatedTodo = todoService.updateTodo(todo, token);

        assertEquals(todo, updatedTodo);
        assertEquals(username, todo.getUsername());
        verify(loginDTORepository, times(1)).findByToken(token);
        verify(todoRepository, times(1)).save(todo);
    }

    @Test
    public void testRemoveTodo() {

        long todoId = 1;

        todoService.removeTodo(todoId);

        verify(todoRepository, times(1)).deleteById(todoId);
    }
}