export class Login {
    username: string | undefined;
    password: string | undefined;
    token: string | undefined;
}