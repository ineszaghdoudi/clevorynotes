export class Todo {
    id: number | undefined;
    title: string | undefined;
    description: string | undefined;
    dueDate: Date | undefined;
    creationDate: Date | undefined;
    completed: boolean | undefined;
    category: string | undefined;
    priority: number | undefined;
    reminderDateTime: Date | undefined;
    token!: string;
}