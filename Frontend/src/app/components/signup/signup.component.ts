import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Signup } from 'src/app/models/Signup';
import { SignupService } from 'src/app/services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  signup: Signup = new Signup();
signupForm: any;

  constructor(
    private router: Router,
    private signupservice: SignupService
  ){}

  userSignup() {
    const passwordPattern = /^(?=.*[A-Za-z])(?=.*\d).{8,}$/;
    //console.log(this.signup);

    if (!this.signup.username) {
      alert("Username is required.");
      return;
    }

    if (!this.signup.password) {
      alert("Password is required.");
      return; 
    }

    if (!this.signup.password || !this.signup.verifypassword) {
      alert("Password verification is required.");
      return;
    }
  

    if (this.signup.password !== this.signup.verifypassword) {
      alert("Password and Password Verification do not match");
      return; 
      
    }else if (!passwordPattern.test(this.signup.password ?? '')) {
      alert("Password must have at least 8 characters with letters and numbers.");
      return; 
    }
  
  
    this.signupservice.signup(this.signup).subscribe(
      data => {
        alert("Registration successful");
        this.router.navigate(['/login']);
      },
      error => {
        alert("Sorry, Username " + this.signup.username +" is already in use. Please verify your information.");
      }
    );
  }
  

  gotoLogin() {
    this.router.navigate(['/login']);
  }

  openLoginInfo() {
    const bForm = document.querySelector('.b-form') as HTMLElement;
    const boxForm = document.querySelector('.box-form') as HTMLElement;
    const boxInfo = document.querySelector('.box-info') as HTMLElement;

    if (bForm && boxForm && boxInfo) {
      bForm.style.opacity = "0.01";
      boxForm.style.left = "-37%";
      boxInfo.style.right = "-37%";
    }
    
  }


  closeLoginInfo() {
    const bForm = document.querySelector('.b-form') as HTMLElement;
    const boxForm = document.querySelector('.box-form') as HTMLElement;
    const boxInfo = document.querySelector('.box-info') as HTMLElement;

    if (bForm && boxForm && boxInfo) {
      bForm.style.opacity = "1";
      boxForm.style.left = "0px";
      boxInfo.style.right = "-5px";
    }
  }

  openLogin() {
    this.openLoginInfo();
  }

  closeLogin() {
    this.closeLoginInfo();
  }

}
