import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CalendarOptions } from '@fullcalendar/core';
import { CookieService } from 'ngx-cookie-service';
import { Login } from 'src/app/models/Login';
import { Todo } from 'src/app/models/Todo';
import { LoginService } from 'src/app/services/login.service';
import { TodoService } from 'src/app/services/todo.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FullCalendarComponent } from '@fullcalendar/angular';

declare var bootstrap: any;

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  login: Login = new Login();

  todo: Todo = new Todo();
  formModalAdd: any;
  formModalEdit: any;
  todoDetail!: FormGroup;
  todoList: Todo[] = [];
  username!: string ;
  token !: string;


  //CALENDAR

  @ViewChild('fullcalendar') fullcalendar!: FullCalendarComponent; 
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    plugins: [dayGridPlugin],
    events: [] 
  };
  calendarEvents: any;

  constructor(
    private formBuilder: FormBuilder,
    private todoService: TodoService,
    private router: Router,
    public loginService: LoginService,
    private cookieService: CookieService
  ) {  }

  ngOnInit(): void {

    this.username = this.loginService.username;
    this.getAllTodos();
    this.updateCalendarEvents();

   

    this.formModalAdd = new bootstrap.Modal(document.getElementById('addTodo') as HTMLElement);
    this.formModalEdit = new bootstrap.Modal(document.getElementById('editTodo') as HTMLElement);

    this.todoDetail = this.formBuilder.group({
      id: [''],
      title: [''],
      description: [''],
      dueDate: [''],
      completed: [''],
      category: [''],
      priority: [''],
      reminderDateTime: [''],
      username: ['']
    });
  }



  addTodo() {
    //console.log(this.todo);
    const token = this.loginService.accessToken;
    //console.log(token);
    if (token !== undefined) {
      this.todoService.addTodo(this.todo, token).subscribe(
        data => {
          this.getAllTodos();
          this.router.navigate(['/home']);
        },
        error => alert("Sorry couldn't add")
      );
    } else {
      console.log("Access token is undefined.");
    }
  }

  getAllTodos() {
    let token = this.cookieService.get('token');
    if (token === '') {
      token = this.loginService.accessToken;
    } else {
      this.loginService.accessToken = token;
    }

    if (token !== undefined) {
      this.updateCalendarEvents();
      this.todoService.getAllTodos(token).subscribe(
        res => {
          this.todoList = res;
          this.updateCalendarEvents();
        },
        err => {
          console.log("Error retrieving all Todos!");
        }
      );
    } else {
      console.log("Access token is undefined.");
    }
  }

  editTodo(todo: Todo) {
    this.todoDetail.controls['id'].setValue(todo.id);
    this.todoDetail.controls['title'].setValue(todo.title);
    this.todoDetail.controls['description'].setValue(todo.description);
    this.todoDetail.controls['dueDate'].setValue(todo.dueDate);
    this.todoDetail.controls['completed'].setValue(todo.completed);
    this.todoDetail.controls['category'].setValue(todo.category);
    this.todoDetail.controls['priority'].setValue(todo.priority);
    this.todoDetail.controls['reminderDateTime'].setValue(todo.reminderDateTime);
  }

  updateTodo() {
    this.todo.id = this.todoDetail.value.id;
    this.todo.title = this.todoDetail.value.title;
    this.todo.description = this.todoDetail.value.description;
    this.todo.dueDate = this.todoDetail.value.dueDate;
    this.todo.completed = this.todoDetail.value.completed;
    this.todo.category = this.todoDetail.value.category;
    this.todo.priority = this.todoDetail.value.priority;
    this.todo.reminderDateTime = this.todoDetail.value.reminderDateTime;

    this.todoService.updateTodo(this.todo, this.loginService.accessToken).subscribe(
      res => {
        console.log(res);
        this.getAllTodos();
        this.cookieService.set('token', this.loginService.accessToken);
      },
      err => {
        console.log(err);
        alert("Sorry todo wasn't edited");
      }
    );
  }

  deleteTodo(todo: Todo) {
    this.todoService.deleteTodo(todo).subscribe(
      res => {
        console.log(res);
        this.getAllTodos();
      },
      err => {
        console.log(err);
        alert("Sorry unable to delete");
      }
    );
  }


  userLogout() {
   this.login.username= this.username;
    this.loginService.logout(this.login).subscribe(
      () => {
        console.log(this.login);
        this.loginService.accessToken = ''; 
        this.router.navigate(['/login']); 
      },
      (error) => {
        console.error('Logout error:', error);

      }
    );
  }

  toggleDone(todo: Todo): void {
    todo.completed = !todo.completed;
  }

  openModal() {
    this.formModalAdd.show();
  }

  openEditModal() {
    this.formModalEdit.show();
  }

  updateCalendarEvents(): void {
    this.calendarOptions.events = [];


    for (const todo of this.todoList) {
      this.calendarOptions.events.push({
        title: todo.title,
        date: todo.dueDate
      });
    }

  }
}
