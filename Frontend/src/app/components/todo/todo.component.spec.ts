import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TodoComponent } from './todo.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FullCalendarModule } from '@fullcalendar/angular'; // Import the FullCalendarModule
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // Import the FormsModule and ReactiveFormsModule

describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TodoComponent],
      imports: [HttpClientTestingModule, FullCalendarModule, FormsModule, ReactiveFormsModule], // Add FullCalendarModule, FormsModule, and ReactiveFormsModule to the imports
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });
});
