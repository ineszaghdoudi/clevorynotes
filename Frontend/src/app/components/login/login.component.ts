import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from 'src/app/models/Login';
import { LoginService } from 'src/app/services/login.service';
import { TodoComponent } from '../todo/todo.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  login: Login = new Login();
  cookieService: any;
  

  constructor(
    public router: Router,
    private loginservice: LoginService
  ){}

  
  userLogin() {
    console.log(this.login);
    this.loginservice.login(this.login).subscribe(data=>{
      //console.log(data)
      //alert("Login successful");
      this.router.navigate(['/home']);
    }, error=> alert("Sorry your username or password are incorrect"));
  }



  gotoSignup() {
    this.router.navigate(['/signup']);
  }

  openLoginInfo() {
    const bForm = document.querySelector('.b-form') as HTMLElement;
    const boxForm = document.querySelector('.box-form') as HTMLElement;
    const boxInfo = document.querySelector('.box-info') as HTMLElement;

    if (bForm && boxForm && boxInfo) {
      bForm.style.opacity = "0.01";
      boxForm.style.left = "-37%";
      boxInfo.style.right = "-37%";
    }
    
  }


  closeLoginInfo() {
    const bForm = document.querySelector('.b-form') as HTMLElement;
    const boxForm = document.querySelector('.box-form') as HTMLElement;
    const boxInfo = document.querySelector('.box-info') as HTMLElement;

    if (bForm && boxForm && boxInfo) {
      bForm.style.opacity = "1";
      boxForm.style.left = "0px";
      boxInfo.style.right = "-5px";
    }
  }

  openLogin() {
    this.openLoginInfo();
  }

  closeLogin() {
    this.closeLoginInfo();
  }

}
