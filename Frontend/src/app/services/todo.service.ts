import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from '../models/Todo';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  apiUrl = environment.apiUrlTodo;
  private addUrl = `${this.apiUrl}/clevery/todo/add-todo`;
  private getAllUrl = `${this.apiUrl}/clevery/todo/retrieve-all-todos`;
  private editUrl = `${this.apiUrl}/clevery/todo/update-todo`;
  private deleteUrl = `${this.apiUrl}/clevery/todo/remove-todo`;

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  
  addTodo(todo: Todo, token :any = this.cookieService.get('token')): Observable<Object> {
    
    return this.http.post(`${this.addUrl}/${token}`, todo);
  }

  getAllTodos(token = this.cookieService.get('token')): Observable<Todo[]> {

    return this.http.get<Todo[]>(`${this.getAllUrl}/${token}`);
  }

  updateTodo(todo: Todo, token = this.cookieService.get('token')): Observable<Todo> {

    return this.http.put<Todo>(`${this.editUrl}/${token}`, todo);
  }

  deleteTodo(todo: Todo): Observable<Todo> {
    const token = this.cookieService.get('token');
    return this.http.delete<Todo>(`${this.deleteUrl}/${todo.id}`);
  }
}
