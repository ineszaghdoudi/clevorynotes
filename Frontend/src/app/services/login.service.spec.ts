import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http'; 
import { LoginService } from './login.service';

describe('LoginService', () => {
  let service: LoginService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule], 
      providers: [LoginService],
    });

    service = TestBed.inject(LoginService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });


  it('should login successfully', () => {
    const loginData = { username: 'testuser', password: 'testpassword', token: 'sampleAccessToken' };
    const expectedResponse = { username: 'testuser', accessToken: 'sampleAccessToken' };

    service.login(loginData).subscribe(() => {
      expect(service.username).toBe(expectedResponse.username);
      expect(service.accessToken).toBe(expectedResponse.accessToken);
    });

    const req = httpTestingController.expectOne(service.urlLogin);
    expect(req.request.method).toBe('POST');
    req.flush(expectedResponse);
  });

  it('should logout successfully', () => {
    const loginData = { username: 'testuser', password: 'testpassword', token: 'sampleAccessToken' };
    const expectedResponse = 'Logout successful';

    service.logout(loginData).subscribe((response) => {
      expect(response).toBe(expectedResponse);
    });

    const req = httpTestingController.expectOne(service.urlLogout);
    expect(req.request.method).toBe('POST');
    req.flush(expectedResponse);
  });

  it('should return true if user is logged in', () => {
    service.accessToken = 'sampleAccessToken';
    const isLoggedIn = service.isLoggedIn();
    expect(isLoggedIn).toBeTruthy();
  });

  it('should return false if user is not logged in', () => {
    service.accessToken = "";
    const isLoggedIn = service.isLoggedIn();
    expect(isLoggedIn).toBeFalsy();
  });
});
