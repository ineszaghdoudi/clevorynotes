import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CookieService } from 'ngx-cookie-service';
import { TodoService } from './todo.service';
import { Todo } from '../models/Todo';

describe('TodoService', () => {
  let service: TodoService;
  let httpMock: HttpTestingController;
  let cookieService: CookieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CookieService]
    });
    service = TestBed.inject(TodoService);
    httpMock = TestBed.inject(HttpTestingController);
    cookieService = TestBed.inject(CookieService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a new todo', () => {
    const dummyToken = 'dummyToken';
    const dummyTodo: Todo = {
      id: 1, title: 'Test Todo', completed: false,
      description: undefined,
      dueDate: undefined,
      creationDate: undefined,
      category: undefined,
      priority: undefined,
      reminderDateTime: undefined,
      token: ''
    };

    service.addTodo(dummyTodo, dummyToken).subscribe((response) => {
      expect(response).toEqual(dummyTodo);
    });

    const req = httpMock.expectOne(`${service.apiUrl}/clevery/todo/add-todo/${dummyToken}`);
    expect(req.request.method).toBe('POST');
    req.flush(dummyTodo);
  });

  it('should retrieve all todos', () => {
    const dummyToken = 'dummyToken';
    const dummyTodos: Todo[] = [
      {       
        id: 1, title: '1 Todo', completed: true,
        description: undefined,
        dueDate: undefined,
        creationDate: undefined,
        category: undefined,
        priority: undefined,
        reminderDateTime: undefined,
        token: '' 
    },
      { 
        id: 2, title: '2 Todo', completed: true,
        description: undefined,
        dueDate: undefined,
        creationDate: undefined,
        category: undefined,
        priority: undefined,
        reminderDateTime: undefined,
        token: '' 
       },
    ];

    service.getAllTodos(dummyToken).subscribe((todos) => {
      expect(todos).toEqual(dummyTodos);
    });

    const req = httpMock.expectOne(`${service.apiUrl}/clevery/todo/retrieve-all-todos/${dummyToken}`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyTodos);
  });

  it('should update a todo', () => {
    const dummyToken = 'dummyToken';
    const dummyTodo: Todo = {
      id: 1, title: 'Updated Todo', completed: true,
      description: undefined,
      dueDate: undefined,
      creationDate: undefined,
      category: undefined,
      priority: undefined,
      reminderDateTime: undefined,
      token: ''
    };

    service.updateTodo(dummyTodo, dummyToken).subscribe((response) => {
      expect(response).toEqual(dummyTodo);
    });

    const req = httpMock.expectOne(`${service.apiUrl}/clevery/todo/update-todo/${dummyToken}`);
    expect(req.request.method).toBe('PUT');
    req.flush(dummyTodo);
  });

  it('should delete a todo', () => {
    const dummyToken = 'dummyToken';
    const dummyTodo: Todo = {
      id: 1, title: 'Test Todo', completed: false,
      description: undefined,
      dueDate: undefined,
      creationDate: undefined,
      category: undefined,
      priority: undefined,
      reminderDateTime: undefined,
      token: ''
    };

    service.deleteTodo(dummyTodo).subscribe((response) => {
      expect(response).toEqual(dummyTodo);
    });

    const req = httpMock.expectOne(`${service.apiUrl}/clevery/todo/remove-todo/${dummyTodo.id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(dummyTodo);
  });
});
