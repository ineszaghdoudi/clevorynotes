import { Injectable } from '@angular/core';
import { Signup } from '../models/Signup';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  apiUrl = environment.apiUrlAuthentification;
  private url=`${this.apiUrl}/clevery/auth/register`;
  constructor(private http: HttpClient) { }

  signup(signup: Signup): Observable<object> {
    return this.http.post(`${this.url}`, signup);
  }
}
