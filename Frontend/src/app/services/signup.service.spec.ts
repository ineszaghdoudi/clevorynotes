import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SignupService } from './signup.service';
import { Signup } from '../models/Signup';

describe('SignupService', () => {
  let service: SignupService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SignupService],
    });

    service = TestBed.inject(SignupService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });


  it('should send a POST request to the server when signing up', () => {
    const mockSignup: Signup = {
      username: 'TestUser',
      password: 'Pass',
      verifypassword: 'Pass'
    };

    const expectedResponse = { message: 'User successfully registered' };

    service.signup(mockSignup).subscribe((response: any) => {
      expect(response).toEqual(expectedResponse);
    });
    

    const req = httpMock.expectOne(`${service.apiUrl}/clevery/auth/register`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(mockSignup);

    req.flush(expectedResponse);
  });


});
