import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { Login } from '../models/Login';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  apiUrl = environment.apiUrlAuthentification;
  public urlLogin = `${this.apiUrl}/clevery/auth/login`;
  public urlLogout = `${this.apiUrl}/clevery/auth/logout`;
  
  public accessToken!: string ;
  public username!: string ;

  constructor(private http: HttpClient) { }

  login(login: Login): Observable<void> {
    return this.http.post<{ username: string, accessToken: string }>(this.urlLogin, login).pipe(
      map(response => {
        this.username = response.username;
        this.accessToken = response.accessToken;
        console.log(this.accessToken);
      })
    );
  }

  logout(login: Login): Observable<string> {
    return this.http.post(this.urlLogout, login, { responseType: 'text' });
  }

  isLoggedIn(): boolean {
    return !!this.accessToken;
  }
}
