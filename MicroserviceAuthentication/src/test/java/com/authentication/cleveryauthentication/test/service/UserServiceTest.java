package com.authentication.cleveryauthentication.test.service;

import com.authentication.cleveryauthentication.document.User;
import com.authentication.cleveryauthentication.repository.UserRepository;
import com.authentication.cleveryauthentication.service.UserManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserManager userManager;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateUser() {

        User user = new User();
        user.setUsername("testuser");
        user.setPassword("password");

        when(passwordEncoder.encode("password")).thenReturn("encodedPassword");
        when(userRepository.save(any(User.class))).thenReturn(user);

        userManager.createUser(user);

        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void testLoadUserByUsername_UserExists() {

        String username = "testuser";
        User user = new User();
        user.setUsername(username);
        user.setPassword("encodedPassword");

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        User result = (User) userManager.loadUserByUsername(username);

        verify(userRepository, times(1)).findByUsername(username);

    }

    @Test
    public void testLoadUserByUsername_UserNotExists() {

        String username = "testuser";
        lenient().when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

    }


    @Test
    public void testDeleteUser_UserNotExists() {

        String username = "nonexistentuser";
        lenient().when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        userManager.deleteUser(username);

        verify(userRepository, never()).delete(any(User.class));
    }



}