package com.authentication.cleveryauthentication.publisher;

import com.authentication.cleveryauthentication.dto.LoginDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
public class RabbitMQProducer {

    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.routing.key}")
    private String routingKey;

    @Value("${rabbitmq.exchange.name.logout}")
    private String exchange_logout;

    @Value("${rabbitmq.routing.key.logout}")
    private String routingKey_logout;

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQProducer.class);
    private RabbitTemplate rabbitTemplate;

    public RabbitMQProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    public void sendMessage(LoginDTO user){
        LOGGER.info(String.format("Json message sent -> %s", user.toString()));

        rabbitTemplate.convertAndSend(exchange, routingKey, user);
    }

    public void sendLogoutMessage(LoginDTO user){
        LOGGER.info(String.format("Json Logout message sent -> %s", user.toString()));

        rabbitTemplate.convertAndSend(exchange_logout, routingKey_logout, user);
    }

}