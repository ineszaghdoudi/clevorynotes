package com.authentication.cleveryauthentication.web;

import com.authentication.cleveryauthentication.controller.MessageController;
import com.authentication.cleveryauthentication.document.User;
import com.authentication.cleveryauthentication.dto.LoginDTO;
import com.authentication.cleveryauthentication.dto.SignupDTO;
import com.authentication.cleveryauthentication.dto.TokenDTO;
import com.authentication.cleveryauthentication.repository.UserRepository;
import com.authentication.cleveryauthentication.security.TokenGenerator;
import com.authentication.cleveryauthentication.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

@RestController
@RequestMapping("/clevery/auth")
@CrossOrigin(origins = "*")
public class AuthController {
    @Autowired
    UserDetailsManager userDetailsManager;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserManager userManager;
    @Autowired
    TokenGenerator tokenGenerator;
    @Autowired
    DaoAuthenticationProvider daoAuthenticationProvider;
    @Autowired
    @Qualifier("jwtRefreshTokenAuthProvider")
    JwtAuthenticationProvider refreshTokenAuthProvider;

    @Autowired
    private MessageController controller;

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody SignupDTO signupDTO) {
        if (userRepository.existsByUsername(signupDTO.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username already exists");
        }

        User user = new User(signupDTO.getUsername(), signupDTO.getPassword());
        userDetailsManager.createUser(user);

        Authentication authentication = UsernamePasswordAuthenticationToken.authenticated(user, signupDTO.getPassword(), Collections.EMPTY_LIST);

        return ResponseEntity.ok(tokenGenerator.createToken(authentication));
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginDTO loginDTO) {

        Authentication authentication = daoAuthenticationProvider.authenticate(UsernamePasswordAuthenticationToken.unauthenticated(loginDTO.getUsername(), loginDTO.getPassword()));
        TokenDTO token = tokenGenerator.createToken(authentication);
        loginDTO.setToken(token.getAccessToken());
        loginDTO.setPassword(null);
        controller.sendMessage(loginDTO);
        return ResponseEntity.ok(tokenGenerator.createToken(authentication));
    }




    @PostMapping("/logout")
    public ResponseEntity<String> logout(@RequestBody LoginDTO loginDTO, HttpServletRequest request, HttpServletResponse response) {

        User user = new User(); // Replace this with your actual authentication logic

        if (user != null) {
            // Create a custom Authentication object using the authenticated User object
            Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());

            new SecurityContextLogoutHandler().logout(request, response, authentication);
            request.getSession().invalidate();
            controller.sendLogoutMessage(loginDTO);
            return ResponseEntity.ok("Logout successful");

        } else {
            return ResponseEntity.badRequest().body("Invalid credentials. Logout not applicable.");
        }
    }





    @PostMapping("/token")
    public ResponseEntity token(@RequestBody TokenDTO tokenDTO) {
        Authentication authentication = refreshTokenAuthProvider.authenticate(new BearerTokenAuthenticationToken(tokenDTO.getRefreshToken()));
        Jwt jwt = (Jwt) authentication.getCredentials();

        return ResponseEntity.ok(tokenGenerator.createToken(authentication));
    }


}







