package com.authentication.cleveryauthentication.dto;

import com.authentication.cleveryauthentication.document.User;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.mongodb.core.index.Indexed;

@Builder
@Data
public class UserDTO {
    private String id;

    @Indexed(unique = true)
    private String username;

    @NonNull
    private String password;

    public static UserDTO from(User user) {
        return builder()
                .id(user.getId())
                .username(user.getUsername())
                .build();
    }
}
