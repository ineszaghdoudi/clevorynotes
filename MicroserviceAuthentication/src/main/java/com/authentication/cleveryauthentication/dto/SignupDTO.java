package com.authentication.cleveryauthentication.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
public class SignupDTO {
    @Id
    @Indexed(unique = true, background = true)
    private String username;
    private String password;
}
