package com.authentication.cleveryauthentication.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;

@Setter
@Getter
public class LoginDTO {
    private String id;
    @Indexed(unique = true)
    private String username;
    private String password;
    private String token;
}