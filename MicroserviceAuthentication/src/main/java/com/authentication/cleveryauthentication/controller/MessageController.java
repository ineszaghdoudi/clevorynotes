package com.authentication.cleveryauthentication.controller;

import com.authentication.cleveryauthentication.dto.LoginDTO;
import com.authentication.cleveryauthentication.publisher.RabbitMQProducer;
import com.authentication.cleveryauthentication.security.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rabbitmq")
public class MessageController {
    private RabbitMQProducer producer;

    @Autowired
    TokenGenerator tokenGenerator;

    public MessageController(RabbitMQProducer producer) {
        this.producer = producer;
    }


    @PostMapping("/publish")
    public ResponseEntity<String> sendMessage(@RequestBody LoginDTO user) {
        producer.sendMessage(user);
        return ResponseEntity.ok("Json message sent to RabbitMQ ...");
    }

    @PostMapping("/logout")
    public ResponseEntity<String> sendLogoutMessage(@RequestBody LoginDTO user) {
        producer.sendLogoutMessage(user);
        return ResponseEntity.ok("Json message sent Logout to RabbitMQ ...");
    }

}
